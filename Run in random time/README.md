## How to use
This is a simple script to run a command in a random time after this script has triggered by cron. Yes, maybe it's a useless script, but I need it for some things.

With it you can set a max delay time (in hours, but it's passed to seconds) and how many days as maximum you want to "skip" this execution.

With this values, the script first check if today will be executed. I case that decided to not execute, checks if the execution has been skipped more than maximum days. In this case the process will be executed. If not, adds 1 to skips and stops the execution.

In case that the execution must be run, takes a random delay time between the initial time (triggered by cron) and the maximum delay and sleeps the execution this time. Then, runs the execution and finish.

### Bash script

Create a file with the following code:

```bash
#!/bin/bash

# Runs a command in a random time between X minutes after run the cronjob

# Delay in hours
DELAY=1
# Max days to skip
MAX_SKIP=2

RUNDIR=/path/to/run/dir
cd $RUNDIR
touch skipped.log
skips=0 #Only for the first execution when skipped.log is empty
run=$(($RANDOM%2))
#Run only if 1
if [[ $run -eq 0 ]]; then
  while read -r line; do
    # Check if skipped more than X days
    skips=$line
    if [[ $skips -lt $MAX_SKIP ]]; then
      echo "Skiped run for $skips times"
      skips=$((skips+1))
      echo $skips > skipped.log
      exit 0
    fi
  done < skipped.log
fi
maxdelay=$(($DELAY*60))  # X hours converted to minutes
delay=$(($RANDOM%maxdelay)) # pick a random delay
(sleep $((delay*60)); YOUR_COMMAND_OR_ANOTHER_SCRIPT) & # background a subshell to wait, then run the command
echo 0 > skipped.log
echo "Done"
```

Replace the /path/to/run/dir and YOUR_COMMAND_OR_ANOTHER_SCRIPT by your values, and of course the values for DELAY and MAX_SKIP.