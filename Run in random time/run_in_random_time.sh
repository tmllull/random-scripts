#!/bin/bash

# Runs a command in a random time between X minutes after run the cronjob

# Delay in hours
DELAY=1
# Max days to skip
MAX_SKIP=2

RUNDIR=/path/to/dir
cd $RUNDIR
touch skipped.log
skips=0 #Only for the first execution when skipped.log is empty
run=$(($RANDOM%2))
#Run only if 1
if [[ $run -eq 0 ]]; then
  while read -r line; do
    # Check if skipped more than X days
    skips=$line
    if [[ $skips -lt $MAX_SKIP ]]; then
      echo "Skiped run for $skips times"
      skips=$((skips+1))
      echo $skips > skipped.log
      exit 0
    fi
  done < skipped.log
fi
maxdelay=$(($DELAY*60))  # X hours converted to minutes
delay=$(($RANDOM%maxdelay)) # pick a random delay
(sleep $((delay*60)); YOUR_COMMAND_OR_ANOTHER_SCRIPT) & # background a subshell to wait, then run the command
echo 0 > skipped.log
echo "Done"
