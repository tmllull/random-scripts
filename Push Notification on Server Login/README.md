## How to use

This is a simple script for send a notification using Pushover servcie when someone login into Raspi, but you can replace for the [Send Telegram Message Script](https://gitlab.com/tmllull/random-scripts/-/tree/master/Send%20Telegram%20Message) [Telegram Sender](https://gitlab.com/tmllull/telegram-sender), [Mail Sender](https://gitlab.com/tmllull/mail-sender) if you want. Just use the command to execute these other projects instead the next script when you call it into 'pam.d'.

First of all, we need to create a new App from [Pushover website](https://pushover.net/).

### Bash script

Create a file pushoverLogin.sh with the following code, changing _APPTOKEN_ and _USERTOKEN_ for the tokens generated from Pushover. Depending the way you use for execute (explained at the end), you must comment one or other MSG line:

```bash
#!/bin/bash

#Using PAM
MSG="message=Login from user $PAM_USER"

#Using profile
#MSG="message=Login from user $USER"

if [ "$PAM_TYPE" != "close_session" ]; then
curl -s \
		--form-string "token=APPTOKEN" \
		--form-string "user=USERTOKEN" \
		--form-string "$MSG" \
		https://api.pushover.net/1/messages.json
fi
exit 0
```

Save and close the file.

Now, copy that file to yout scripts folder (or where you want) and change permissions on it to allow execution:

```shell
sudo cp pushoverLogin.sh /path/to/my/scripts
sudo chmod +x /path/to/my/scripts/pushoverLogin.sh
```

### Prepare for launch

Finally, we've 2 way to execute:

1. Edit /etc/pam.d/sshd and add a call to the script like follow. **Take care to add the line just next to "pam_selinux.so close"**:

```bash
SOME CODE
.............
# SELinux needs to be the first session rule.  This ensures that any
# lingering context has been cleared.  Without this it is possible that a
# module could execute code in the wrong domain.
session [success=ok ignore=ignore module_unknown=ignore default=bad]        pam_selinux.so close

# My script
session required pam_exec.so seteuid /path/to/my/scripts/pushoverLogin.sh

# Set the loginuid process attribute.
session    required     pam_loginuid.so

# Create a new session keyring.
session    optional     pam_keyinit.so force revoke
............
MORE CODE
```

2. Edit /etc/profile and add a call to the script at the end

```bash
# /etc/profile: system-wide .profile file for the Bourne shell (sh(1)) and...
...
SOME CODE (NOT CHANGE)
...
# My scripts
/path/to/my/scripts/pushoverLogin.sh >/dev/null 2>/dev/null
```

_The command >/dev/null 2>/dev/null is for redirect the output messages to this directory, and not print on prompt_
