#!/bin/bash

PID=`cat dynamicCron.pid`
echo "Stopping Dynamic Cron..."
kill -9 $PID
echo "Dynamic Cron stopped"