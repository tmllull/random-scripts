#!/bin/bash

if [[ $# != 1 ]]; then
  echo "You must provide a file with defined hours, separated by ',' and without spaces"
  exit 1
fi

HOURS=$(cat $1)
OIFS=$IFS
IFS=','
hours=($HOURS)
i=0
while [[ $i -lt ${#hours[@]} ]]; do
  current_hour=`date +"%S"`
  current_min=`date +"%M"`
  current_sec=`date +"%S"`
  trigger_hour="${hours[$i]}"
  if [[ $trigger_hour == $current_hour ]]; then
    echo "Trigger hour ${hours[$i]} matches with $current_hour. Do stuff"
    ((++i))
  # This is just for force to exit script at specific hour, but usually exits
  # after all hour occurencies triggers (if all works as expected)
  elif [[ $current_hour == "23" && $current_min == "30" && $current_sec == "00" ]]; then
    echo "Custom Cron forced exited"
    exit 0
  fi
done
