#!/bin/bash
echo "Starting Dynamic Cron..."
cd /path/to/dynamicCronScript
nohup ./myDynamicCron.sh definedHours.txt >/dev/null 2>&1 &
_pid=$!
echo "$_pid" > ./dynamicCron.pid
echo "Pid $_pid stored in dynamicCron.pid"