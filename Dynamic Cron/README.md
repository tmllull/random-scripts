# Run a command(s) or other scripts based on a dynamic hours

This script allows to run commands or another scripts based on a different hours defined into a file. The idea is run this main script in background (nohup) via cron, for now at a defined hour depending on the custom trigger hours. For example: if I want to run a command 3 times a day, but I know that these hours are usually after 10:00h and never after 23:00h, I can run my main script at 08:00h and stop it at 23:30h (this stop time can be defined into the script).

A use case can be, for example, combine the best hours to turn on my washing machine (using the script ["Electricity Prices"](https://gitlab.com/tmllull/electricity-prices)) and based on these hours send me a reminder to my mobile. If you want to use the script to get the best electricity hours you need to clean the output before with [this other script](https://gitlab.com/tmllull/electricity-prices/-/blob/master/cleanHours.sh)

1. Create a file (myDynamicCron.sh) with the following content:

```bash
#!/bin/bash

if [[ $# != 1 ]]; then
  echo "You must provide a file with defined hours, separated by ',' and without spaces"
  exit 1
fi

HOURS=$(cat $1)
OIFS=$IFS
IFS=','
hours=($HOURS)
i=0
while [[ $i -lt ${#hours[@]} ]]; do
  current_hour=`date +"%H"`
  current_min=`date +"%M"`
  current_sec=`date +"%S"`
  trigger_hour="${hours[$i]}"
  if [[ $trigger_hour == $current_hour ]]; then
    ########## PUT HERE YOUR CODE ###########
    echo "Trigger hour ${hours[$i]} matches with $current_hour. Do stuff"
    #########################################
    ((++i))
  # This is just for force to exit script at specific hour, but usually exits
  # after all hour occurrencies triggers (if all works as expected)
  elif [[ $current_hour == "23" && $current_min == "30" && $current_sec == "00" ]];
    echo "Custom Cron forced exited"
    exit 0
fi
done
```

NOTE: **This script needs a file with the defined hours as argument (with this we can run multiple processes), separated by ',' without spaces, in 24h format and hours between 1-9 must be defined as 01,02,...**

2. Create a new file (startDynamicCron.sh) with the follow content. This script store the PID of the current Dynamic Cron execution:

```bash
#!/bin/bash
echo "Starting Dynamic Cron..."
cd /path/to/dynamicCronScript
nohup ./myDynamicCron.sh DEFINED_HOURS.txt >/dev/null 2>&1 &
_pid=$!
echo "$_pid" > ./dynamicCron.pid
echo "Pid $_pid stored in dynamicCron.pid"
```

Note: Replace DEFINED_HOURS.txt for the file with your hours (recommended pass the absolute path if the file is not in the execution path).

3. If you want to execute directly, run:

```shell
./startDynamicCron.sh
```

But the idea is to put this script into your crontab and run it at the best hour for your defined hours (as explained at the beginning)

4. (Optional) If we need to stop the Dynamic Cron execution, we can do it easily with the next script (stopDynamicCron.sh). Using the PID stored with the 'startDynamicCron.sh' when can execute this script a stops the process:

```bash
#!/bin/bash

PID=`cat dynamicCron.pid`
echo "Stopping Dynamic Cron..."
kill -9 $PID
echo "Dynamic Cron stopped"
```

**Don't forget to grant execution permissions to all the scripts**
