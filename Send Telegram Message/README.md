## How to use
This is a simple script for send a message to a Telegram Group using a custom bot via BASH

### Bash script

Create a file with the following code:

```bash
#!/bin/bash

if [[ $# -ne 3 ]]
  then
    echo "Invalid parameters"
    exit 1
fi

TOKEN=$1
ID=$2
MESSAGE=$3
ICON=$'\xF0\x9F\x94\x94'
URL="https://api.telegram.org/bot$TOKEN/sendMessage"
curl -s -X POST $URL -d chat_id=$ID -d text="$MESSAGE"

#json=$(jq -anc --arg id "$ID" --arg msg "$MESSAGE" '{"chat_id": $id, "text": $msg, "parse_mode": "html"}')
#curl -X POST -H "Content-Type: application/json" -d "$json" $URL
```

This script needs the bot Token, group ID and Message as arguments:

```shell
./sendToTelegram.sh TOKEN GROUPID MESSAGE
```

NOTE: If you want to send a message using Docker in a easier way, you can found another specific repo: [Telegram Sender](https://gitlab.com/tmllull/telegram-sender)