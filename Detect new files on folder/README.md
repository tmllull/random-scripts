# Detect new files on folder

This is a little script using inotify-tools to make something when detect a new file.

Firts, install inotify-tools

```shell
sudo apt-get install inotify-tools
```

Create a new file:

```shell
nano detect_files.sh
```

Add this code:

```bash
#!/bin/sh
MONITORDIR="/path/to/the/dir/to/monitor/"
inotifywait -m -r -e create --format '%w%f' "${MONITORDIR}" | while read NEWFILE
do
    Make that you want, like send an email, push notification, etc.
done
```

Add execution persmissions:

```shell
sudo chmod +x detect_files.sh
```

And execute it using nohup command, for allow the script running when you logout from your session:

```shell
nohup detect_files.sh &
```
sources:

https://github.com/rvoicilas/inotify-tools/wiki

http://superuser.com/questions/956311/continuously-detect-new-files-with-inotify-tools-within-multiple-directories-r

https://techarena51.com/index.php/inotify-tools-example/

http://stackoverflow.com/questions/18692134/continuously-monitor-a-directory-in-linux-and-notify-when-a-new-file-is-availabl
